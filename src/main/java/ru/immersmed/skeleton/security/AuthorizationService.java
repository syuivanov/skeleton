package ru.immersmed.skeleton.security;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class AuthorizationService {
    //входной пример role: 'врач,робот'
    public boolean checkRole(UserDetails principal, String role) {
        List<String> roles = Arrays.asList(role.split(","));
        ApplicationUser applicationUser = (ApplicationUser) principal;
        return applicationUser.getAuthorityList().keySet().stream().anyMatch(roles::contains);
    }

    //входной пример check: 'врач{удаление ролей, добавление ролей};регистратор{удаление ролей, добавление ролей}'
    public boolean checkPermission(UserDetails principal, String check) {
        ApplicationUser applicationUser = (ApplicationUser) principal;

        List<String> parts = Arrays.asList(check.split(";"));
        Map<String, List<String>> authorityList = new HashMap<>();
        parts.forEach(s -> {
            String role = s.substring(0, s.indexOf("{"));
            List<String> permissions = Arrays.asList(s.substring(s.indexOf("{") + 1, s.length() - 1).split(","));
            authorityList.put(role, permissions);
        });

        return authorityList.keySet().stream().anyMatch(s -> {
            List<String> permissions = applicationUser.getAuthorityList().get(s);
            if (permissions != null && !permissions.isEmpty()) {
                return permissions.stream().anyMatch(s1 -> authorityList.get(s).contains(s1));
            }
            return false;
        });

    }
}
