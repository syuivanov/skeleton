package ru.immersmed.skeleton.caller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import ru.immersmed.skeleton.order.model.Order;

import java.util.List;

@Service
public class OrderCallerService {
    private static final Logger logger = LoggerFactory.getLogger(OrderCallerService.class);

    private final RestTemplate clientRestTemplate;

    public OrderCallerService(RestTemplate clientRestTemplate) {
        this.clientRestTemplate = clientRestTemplate;
    }

    public Order createOrder(Order order) {
        HttpEntity<Order> request = new HttpEntity<>(order);
        Order newOrder = clientRestTemplate.postForObject("/", request, Order.class);
        logger.info("Get created order from external service");
        return newOrder;
    }

    public List<Order> findOrder(String content) {
        UriComponentsBuilder builder = UriComponentsBuilder.newInstance()
                .queryParam("content", content);
        return clientRestTemplate.exchange("/search" + builder.toUriString(), HttpMethod.GET,
                null, new ParameterizedTypeReference<List<Order>>() {
                }).getBody();
    }

}
