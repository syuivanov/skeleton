package ru.immersmed.skeleton.order.service;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import ru.immersmed.skeleton.order.model.Order;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

@Service
public class OrderService {
    private static final List<Order> orders = new ArrayList<>();

    @Cacheable("order")
    public List<Order> getOrderList() {
        return orders;
    }

    @CacheEvict(value = "order", allEntries = true)
    public Order createOrder(Order fromOrder) {
        Order order = new Order(new Random().nextInt(1000), fromOrder.getContent());
        orders.add(order);
        return order;
    }

    @Cacheable("order")
    public List<Order> findByContent(String content) {
        return orders.stream().filter(order -> order.getContent().equals(content)).collect(Collectors.toList());
    }

}
