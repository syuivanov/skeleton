package ru.immersmed.skeleton.order.service.eventbus;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import ru.immersmed.skeleton.order.model.Order;
import ru.immersmed.skeleton.order.model.OrderCreatedEvent;

@Service
public class OrderEventListenerService {
    private static final Logger logger = LoggerFactory.getLogger(OrderEventListenerService.class);

    @EventListener
    @Async
    public void getEventOrderCreated(OrderCreatedEvent orderCreatedEvent) {
        Order order = (Order) orderCreatedEvent.getSource();
        logger.info("New order was created: id = " + order.getId() + " ,content = " + order.getContent());
    }
}
