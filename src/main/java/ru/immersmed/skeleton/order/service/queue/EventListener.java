package ru.immersmed.skeleton.order.service.queue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.AmqpRejectAndDontRequeueException;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;
import ru.immersmed.skeleton.order.model.Order;

@Service
public class EventListener {
    private static final Logger logger = LoggerFactory.getLogger(EventListener.class);

    @RabbitListener(queues = "${rabbit.order.event.queue}")
    public void receiveMessage(Order order) {
        try {
            logger.info("Received new order from rabbit with id: {} and content: {}", order.getId(), order.getContent());
        } catch (Exception ex) {
            throw new AmqpRejectAndDontRequeueException("wrong");
        }
    }

}
