package ru.immersmed.skeleton.order.service.eventbus;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import ru.immersmed.skeleton.order.model.Order;
import ru.immersmed.skeleton.order.model.OrderCreatedEvent;

@Service
public class OrderEventBus {
    private final ApplicationEventPublisher applicationEventPublisher;

    public OrderEventBus(ApplicationEventPublisher applicationEventPublisher) {
        this.applicationEventPublisher = applicationEventPublisher;
    }

    @Async
    public void createNewOrder(Order order) {
        applicationEventPublisher.publishEvent(new OrderCreatedEvent(order));
    }

}
