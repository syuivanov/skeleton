package ru.immersmed.skeleton.order.service.scheduler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class ScheduledOrderService {
    private static final Logger logger = LoggerFactory.getLogger(ScheduledOrderService.class);

    // "0 0 * * * *" = the top of every hour of every day.
    // "*/10 * * * * *" = every ten seconds.
    // "0 0 8-10 * * *" = 8, 9 and 10 o'clock of every day.
    // "0 0/30 8-10 * * *" = 8:00, 8:30, 9:00, 9:30 and 10 o'clock every day.
    // "0 0 9-17 * * MON-FRI" = on the hour nine-to-five weekdays
    // "0 0 0 25 12 ?" = every Christmas Day at midnight
    //@Scheduled(cron = "*/10 * * * * *")
    @Scheduled(initialDelay = 5000, fixedRate = 5000)
    public void reportCurrentTimeRate() throws InterruptedException {
        logger.info("Scheduled task is done, day&time is {}", LocalDateTime.now());
    }

}
