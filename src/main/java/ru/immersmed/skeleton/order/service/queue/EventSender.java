package ru.immersmed.skeleton.order.service.queue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.immersmed.skeleton.order.model.Order;

@Service
public class EventSender {
    private static final Logger logger = LoggerFactory.getLogger(EventSender.class);

    @Value("${rabbit.order.event.exchange}")
    String topicExchangeName;

    private final RabbitTemplate rabbitTemplate;

    public EventSender(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    public void sendEvent(Order order) {
        logger.info("Sending new order to rabbit");
        this.rabbitTemplate.convertAndSend(topicExchangeName, String.valueOf(order.getId()), order);
        logger.info("Sent new order to rabbit");
    }
}
