package ru.immersmed.skeleton.order.model;

import org.springframework.context.ApplicationEvent;

public class OrderCreatedEvent extends ApplicationEvent {

    public OrderCreatedEvent(Object source) {
        super(source);
    }

}
