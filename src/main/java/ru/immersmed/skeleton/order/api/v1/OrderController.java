package ru.immersmed.skeleton.order.api.v1;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import ru.immersmed.skeleton.order.model.Order;
import ru.immersmed.skeleton.order.service.eventbus.OrderEventBus;
import ru.immersmed.skeleton.order.service.OrderService;
import ru.immersmed.skeleton.order.service.queue.EventSender;

import java.util.List;

@RestController
@RequestMapping("/v1/order")
@CrossOrigin
public class OrderController {
    private static final Logger logger = LoggerFactory.getLogger(OrderController.class);

    private final OrderEventBus orderEventBus;
    private final OrderService orderService;
    private final EventSender eventSender;

    public OrderController(OrderEventBus orderEventBus, OrderService orderService, EventSender eventSender) {
        this.orderEventBus = orderEventBus;
        this.orderService = orderService;
        this.eventSender = eventSender;
    }

    @PostMapping("")
    public Order createOrder(@RequestBody Order requestOrder) {
        logger.info("Request for creating new Order");
        Order order = orderService.createOrder(requestOrder);
        //rabbitmq
        eventSender.sendEvent(order);
        //spring event bus
        orderEventBus.createNewOrder(order);
        return order;
    }

    @GetMapping("/")
    public List<Order> getOrderList() {
        return orderService.getOrderList();
    }

    @GetMapping("/search")
    public List<Order> getOrderList(@RequestParam("content") String content) {
        return orderService.findByContent(content);
    }

}
