package ru.immersmed.skeleton.user.api.v1;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import ru.immersmed.skeleton.security.ApplicationUser;
import ru.immersmed.skeleton.user.model.User;
import ru.immersmed.skeleton.user.service.UserService;

import java.util.List;

@RestController
@RequestMapping("/v1/user")
@CrossOrigin
public class UserController {
    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("")
    //Требуется наличие любой из ролей {врач, робот}
    @PreAuthorize("@authorizationService.checkRole(principal, 'врач,робот')")
    public User create(@RequestBody User user) {
        ApplicationUser applicationUser = (ApplicationUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        logger.info("User {} starts creating new user", applicationUser.getFio());
        User createdUser = userService.register(user);
        logger.info("User {} created new user with id: {} and username: {}", applicationUser.getFio(), createdUser.getId(), createdUser.getUserName());
        return createdUser;
    }

    @GetMapping("/")
    //Требуется наличие любого из полномочия {поиск,удаление} для роли {врач} или полномочия {удаление} для роли {робот}
    @PreAuthorize("@authorizationService.checkPermission(principal, 'врач{поиск,удаление};робот{удаление}')")
    public List<User> getUsers() {
        return userService.getAll();
    }

    @GetMapping("/{id}")
    public User getUser(@PathVariable Long id) {
        return userService.getById(id);
    }

    @GetMapping("/searches")
    public List<User> getUserByName(@RequestParam String name) {
        return userService.findByUsername(name);
    }

    @GetMapping("/count")
    public Long getNumberByName(@RequestParam String name) {
        return userService.countByName(name);
    }

}
