package ru.immersmed.skeleton.user.db;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import ru.immersmed.skeleton.user.model.User;

import javax.persistence.EntityManager;
import javax.persistence.criteria.*;
import java.util.List;

@Repository
public class UserRepositoryCustomImpl implements UserRepositoryCustom {
    private final EntityManager entityManager;
    private final JdbcTemplate jdbcTemplate;
    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public UserRepositoryCustomImpl(EntityManager entityManager, JdbcTemplate jdbcTemplate, NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.entityManager = entityManager;
        this.jdbcTemplate = jdbcTemplate;
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    public Long countByName(String name) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root<User> user = criteriaQuery.from(User.class);
        Path<String> userName = user.get("userName");
        Predicate predicate = criteriaBuilder.like(userName, name);
        criteriaQuery.select(criteriaBuilder.count(user));
        criteriaQuery.where(predicate);
        return entityManager.createQuery(criteriaQuery).getSingleResult();
    }

    //Работа с БД через jdbcTemplate
    public List<User> getAll() {
        return jdbcTemplate.query(
                "select * from users",
                (rs, rowNum) ->
                        new User(
                                rs.getLong("id"),
                                rs.getString("username"),
                                rs.getString("email"),
                                rs.getTimestamp("created").toLocalDateTime(),
                                rs.getTimestamp("updated").toLocalDateTime()
                        )
        );
    }

    //Работа с БД через namedParameterJdbcTemplate
    public List<User> findByUsername(String userName) {
        return namedParameterJdbcTemplate.query(
                "select * from users where username = :userName",
                new MapSqlParameterSource("userName", userName),
                (rs, rowNum) ->
                        new User(
                                rs.getLong("id"),
                                rs.getString("username"),
                                rs.getString("email"),
                                rs.getTimestamp("created").toLocalDateTime(),
                                rs.getTimestamp("updated").toLocalDateTime()
                        )
        );
    }

}
