package ru.immersmed.skeleton.user.db;

import ru.immersmed.skeleton.user.model.User;

import java.util.List;

public interface UserRepositoryCustom {
    public Long countByName(String name);
    public List<User> getAll();
    public List<User> findByUsername(String userName);
}
