package ru.immersmed.skeleton.user.model;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;
import java.time.LocalDateTime;

@EntityListeners(AuditingEntityListener.class)
@Entity
@Table(name = "users")
public class User extends BaseEntity {

    public User() {
    }

    public User(Long id, String userName, String email, LocalDateTime created, LocalDateTime updated) {
        super(id, created, updated);
        this.userName = userName;
        this.email = email;
    }

    @Column(name = "username")
    private String userName;

    @Column(name = "email")
    private String email;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
