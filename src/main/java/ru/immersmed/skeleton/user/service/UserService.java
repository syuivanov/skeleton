package ru.immersmed.skeleton.user.service;

import org.springframework.stereotype.Service;
import ru.immersmed.skeleton.user.db.UserRepository;
import ru.immersmed.skeleton.user.model.User;

import java.util.List;

@Service
public class UserService {
    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    //Работа с БД через Repository
    public User register(User user) {
        return userRepository.save(user);
    }

    //Работа с БД через jdbcTemplate
    public List<User> getAll() {
        return userRepository.getAll();
    }

    //Работа с БД через namedParameterJdbcTemplate
    public List<User> findByUsername(String userName) {
        return userRepository.findByUsername(userName);
    }

    //Работа с БД через JPQL
    public User getById(Long id) {
        return userRepository.getById(id);
    }

    //Работа с БД через Criteria
    public Long countByName(String name) {
        return userRepository.countByName(name);
    }

}
