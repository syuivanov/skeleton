package ru.immersmed.skeleton;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import java.time.Duration;

@Configuration
public class RestTemplateConfig {

    @Value("${order.url}")
    String orderURL;

    @Bean
    public RestTemplate clientRestTemplate() {
        return new RestTemplateBuilder().
                rootUri(orderURL).
                setConnectTimeout(Duration.ofSeconds(3)).
                setReadTimeout(Duration.ofSeconds(3)).build();
    }

}
