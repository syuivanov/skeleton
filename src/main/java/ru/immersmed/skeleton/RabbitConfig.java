package ru.immersmed.skeleton;

import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableRabbit
public class RabbitConfig {
    @Value("${rabbit.order.event.exchange}")
    String topicExchangeName;
    @Value("${rabbit.order.event.queue}")
    String queue;

    @Bean
    FanoutExchange orderEventExchange() {
        return new FanoutExchange(topicExchangeName);
    }

    @Bean
    FanoutExchange orderEventExceptionExchange() {
        return new FanoutExchange(topicExchangeName + "-exception");
    }

    @Bean
    Queue queue() {
        return QueueBuilder.durable(queue)
                .withArgument("x-dead-letter-exchange", topicExchangeName + "-exception")
                .build();
    }

    @Bean
    Queue queueException() {
        return new Queue(queue + "-exception", true);
    }

    @Bean
    Binding bindingOne(Queue queue, FanoutExchange orderEventExchange) {
        return BindingBuilder.bind(queue).to(orderEventExchange);
    }

    @Bean
    Binding bindingTwo(Queue queueException, FanoutExchange orderEventExceptionExchange) {
        return BindingBuilder.bind(queueException).to(orderEventExceptionExchange);
    }

    @Bean
    public RabbitTemplate rabbitTemplate(ConnectionFactory connectionFactory) {
        RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(producerJackson2MessageConverter());
        return rabbitTemplate;
    }

    @Bean
    public Jackson2JsonMessageConverter producerJackson2MessageConverter() {
        return new Jackson2JsonMessageConverter();
    }

}
