package ru.immersmed.skeleton;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import ru.immersmed.skeleton.user.db.UserRepository;
import ru.immersmed.skeleton.user.model.User;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class JPATests {
    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private UserRepository userRepository;

    @Test
    public void findUserByName() {
        // given
        User user = new User();
        user.setUserName("Petrov Ivan");
        user.setEmail("test@mail.ru");
        Long id = entityManager.persist(user).getId();
        entityManager.flush();

        // when
        User found = userRepository.getById(id);

        // then
        assertThat(found.getUserName()).isEqualTo(user.getUserName());
    }

}
