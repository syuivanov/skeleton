package ru.immersmed.skeleton;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.immersmed.skeleton.user.model.User;
import ru.immersmed.skeleton.user.service.UserService;

import java.time.LocalDateTime;
import java.util.Collections;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MainTests {
    @Mock
    UserService userService;

    @Before
    public void setUp() {
        User user = new User();
        user.setId(1L);
        user.setUserName("Petrov Ivan");
        user.setEmail("test@mail.ru");
        user.setCreated(LocalDateTime.now());
        user.setUpdated(LocalDateTime.now());

        Mockito.when(userService.findByUsername("Petrov Ivan"))
                .thenReturn(Collections.singletonList(user));
    }


    @Test
    public void findUserByName() {
        // given
        String name = "Petrov Ivan";

        // when
        User found = userService.findByUsername(name).get(0);

        // then
        assertThat(found.getId()).isNotNull();
        assertThat(found.getUserName()).isEqualTo(name);
    }

}
